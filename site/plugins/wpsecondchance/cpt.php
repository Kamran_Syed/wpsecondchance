<?php

	add_action( 'init', 'WPSECONDC_add_post_type' );
	add_filter('manage_edit-'.WPSECONDC_CPT_NAME.'_columns' , 'wpsecondc_cpt_name_coloumn',10, 1);
	add_filter('manage_'.WPSECONDC_CPT_NAME.'_posts_custom_column' , 'wpsecondc_cpt_name_coloumn_value',10,  2);
	add_action( 'admin_notices', 'aspk_wpsites_admin_notice' );
	add_action('admin_menu','admin_menu');
	add_action( 'init', 'create_group_taxonomy' );
	add_action( 'pre_get_posts', 'wpsecondc_enable_fps' );
	add_filter( 'get_pages', 'WPSECONDC_add_to_front', 10, 2 );

		function create_group_taxonomy() {
			$labels = array(
				'name'                           => 'Redirect Group',
				'singular_name'                  => 'Redirect Group',
				'search_items'                   => 'Search Redirect Group',
				'all_items'                      => 'All Redirect Groups',
				'edit_item'                      => 'Edit Redirect Group',
				'update_item'                    => 'Update Redirect Group',
				'add_new_item'                   => 'Add New Redirect Group',
				'new_item_name'                  => 'New Redirect Group Name',
				'menu_name'                      => 'Redirect Group',
				'view_item'                      => 'View Redirect Group',
				'popular_items'                  => 'Popular Redirect Group',
				'separate_items_with_commas'     => 'Separate Redirect Groups with commas',
				'add_or_remove_items'            => 'Add or remove Redirect Groups',
				'choose_from_most_used'          => 'Choose from the most used Redirect Group',
				'not_found'                      => 'No Redirect Groups found'
			);

			register_taxonomy(
			'group',
			WPSECONDC_CPT_NAME,
			array(
				'hierarchical' => true,
				'label' => 'Redirect Group',
				'query_var' => true,
				'rewrite' => true
			)
			);
		}

		function WPSECONDC_add_post_type() {
		  // Register CPT 1  

		  $labels = array(

			'name' => _x( WPSECONDC_CPT_LABEL, 'post type general name', WPSECONDC_NAME),

			'singular_name' => _x( WPSECONDC_CPT_LABEL, 'post type singular name', WPSECONDC_NAME),

			'add_new' => _x('Add New', WPSECONDC_CPT_LABEL, WPSECONDC_NAME),

			'add_new_item' => __('Add New '.WPSECONDC_CPT_LABEL, WPSECONDC_NAME),

			'edit_item' => __('Edit '.WPSECONDC_CPT_LABEL, WPSECONDC_NAME),

			'new_item' => __('New '.WPSECONDC_CPT_LABEL, WPSECONDC_NAME),

			'all_items' => __('All '.WPSECONDC_CPT_LABEL.'s', WPSECONDC_NAME),

			'view_item' => __('View '.WPSECONDC_CPT_LABEL, WPSECONDC_NAME),

			'search_items' => __('Search '.WPSECONDC_CPT_LABEL.'s', WPSECONDC_NAME),

			'not_found' =>  __('No '.WPSECONDC_CPT_LABEL.' found', WPSECONDC_NAME),

			'not_found_in_trash' => __('No '.WPSECONDC_CPT_LABEL.' found in Trash', WPSECONDC_NAME), 

			'parent_item_colon' => '',

			'menu_name' => __( 'WP2ndChance', WPSECONDC_NAME)



		  );

		  $args = array(

			'labels' => $labels,

			'public' => true,

			'publicly_queryable' => true,

			'show_ui' => true, 

			'show_in_menu' => true, 

			'query_var' => true,

			'rewrite' => true,

			'capability_type' => 'page',

			'has_archive' => true, 

			'hierarchical' => false,

			'menu_position' => 20,

			'supports' => array( 'title' ),
			
			'taxonomies' => array('category')

		  ); 

		  
		 // 'supports' => array( 'title', 'editor' )

		  register_post_type(WPSECONDC_CPT_NAME, $args);

			flush_rewrite_rules();

		}

		//register_taxonomy( 'platform', 'game', $args );

		function aspk_wpsites_admin_notice(){ 
			$admin_url = admin_url().'edit.php?post_type='.WPSECONDC_NAME;
			$actual_link = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			if($admin_url == $actual_link){
				$url = plugins_url('images/wp_sc_logo.jpg',__FILE__);
				echo "<div style=\"clear:left;width:20em;\"><img style=\"width:20em;\" src=\"$url\"></div>";
			}
		}

		function admin_menu(){
			add_submenu_page('edit.php?post_type='.WPSECONDC_NAME, 'SubPage Campaign', 'SubPage Campaign', 'edit_posts', basename(__FILE__), 'subpage_campaign');
		}

		function update_post_type($new_post_type,$old_post_type){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}posts set  post_type = '{$new_post_type}' where post_type = '{$old_post_type}';";
			$wpdb->query($sql);
		}

		function get_wp_compaing_posts_guid($new_name,$old_name){
			global $wpdb;
			
			$query = "SELECT ID,guid FROM {$wpdb->prefix}posts where post_type = '{$old_name}'"; 
			$guids = $wpdb->get_results($query);
			if(!empty($guids)){
				foreach($guids as $guid){
					
					$id = str_replace($old_name,$new_name,$guid->guid);
					$links[$guid->ID] = $id;
				}
			}
			return $links;
		}

		function update_guids($guids){
			global $wpdb;
			
			foreach($guids as $pid => $value){
				$sql = "UPDATE {$wpdb->prefix}posts set  guid = '{$value}' where ID = {$pid};";
				$wpdb->query($sql);
			}
		}

		function subpage_campaign(){
			if(isset($_POST['add_post_type_name'])){
				$name = $_POST['post_type_name'];
				if(!empty($name)){
					$sanitize_title = sanitize_title($name);
					if($name == $sanitize_title){
						$old_post_type = get_option('_aspk_wpc_post_type');
						update_option('_aspk_wpc_post_type',$name);
						$guids = get_wp_compaing_posts_guid($name,$old_post_type);
						if($guids){
							update_guids($guids);
						}
						update_post_type($name,$old_post_type);
						$message ="Setting has been saved";
						$class = "updated";
						$new_post_type = get_option('_aspk_wpc_post_type');
						$actual_link = admin_url().'edit.php?post_type='.$new_post_type.'&page=cpt.php'; ?>
						<?php
					}else{
						 $message ="Space Not Allowed in Title";
						 $class = "error";
					}
				}
			}
			$old_post_type = get_option('_aspk_wpc_post_type');
			if(empty($old_post_type)){
				update_option('_aspk_wpc_post_type','wp2c');
				$old_post_type = get_option('_aspk_wpc_post_type');
			}
			if(isset($message)){ ?>
				<div style="width:24em;" id="error" class="<?php echo $class; ?>"><?php echo $message; ?></div>
			<?php
			}
			$old_post_type = get_option('_aspk_wpc_post_type');
			if(empty($old_post_type)) $old_post_type = null;
			?>
			<div style="float:left;clear:left;background:#FFFFFF;padding:2em;">
				<h3>SubPage Campaign</h3>
				<form action="" method="post">
					<div style="float:left;clear:left;">
						<div style="float:left;">
							<label>Enter Slug</label>
						</div>
						<div style="margin-left:2em;float:left;">
						 <input required style="width:15em;" type="text" name="post_type_name" value="<?php echo $old_post_type; ?>"/>
						</div>
					</div>
					<div style="margin-top:1em;margin-left:6.7em;float:left;clear:left;">
						<input class="button button-primary" type="submit" name="add_post_type_name" value="Save"/>
					</div>
				</form>
			</div>
			<script>
			setTimeout(function(){ 
				jQuery('#error').hide();
			}, 8000);
			<?php if(!empty($actual_link)){ ?>
				setInterval(function(){ 
					window.location.href = '<?php echo $actual_link; ?>';
				}, 2000);
				
			<?php } ?>
			</script>
			<?php
		}

		function wpsecondc_cpt_name_coloumn($columns){
			foreach($columns as $k => $v){
				if($v == 'Analytics' || $k == 'analytics') continue;
				$new_columns[$k] = $v;
				if($k == 'title'){
					$new_columns['redirect_link'] = 'Redirect Link';
					$new_columns['redirect_url'] = 'Redirect Url';
					$new_columns['traffic_today'] = 'Traffic Today';
					$new_columns['traffic_todate'] = 'Traffic To Date';
				}
			}
			return $new_columns;
		}
		
		function wpsecondc_cpt_name_coloumn_value($column,$post_id){
			$cur_date_obj = new DateTime();
			$current_date = $cur_date_obj->format('Y-m-d');
			$redir_date = get_post_meta($post_id,'_aspk_redirect_post_date',true);
			if($current_date > $redir_date){
				update_post_meta($post_id,'_aspk_redirect_post_count',0);
				update_post_meta($post_id,'_aspk_redirect_post_date',$current_date);
			}
			switch($column){
				case 'redirect_link':
					$permalink = get_permalink($post_id);
					if(!empty($permalink)) echo $permalink;
					break;
				case 'redirect_url':
					$url = get_post_meta($post_id,'_wpsecondc_urlredirect',true);
					if(!empty($url)) echo $url;
					break;
				case 'traffic_today':
					$count = get_post_meta($post_id,'_aspk_redirect_post_count',true);
					if(!empty($count)) echo $count;
					if(empty($count)) echo 0;
					break;
				case 'traffic_todate':
					$count = get_post_meta($post_id,'_aspk_redirect_post_count_to_date',true);
					if(!empty($count)) echo $count;
					if(empty($count)) echo 0;
					break;
			}
		}
		/* 
		add_filter('template_include', 'WPSECONDC_template_include', 1, 1); 

		function WPSECONDC_template_include($template){

			global $wp_query;

			

		  

			if( ( get_query_var('post_type') == WPSECONDC_CPT_NAME ) || ( is_front_page() && is_singular(WPSECONDC_CPT_NAME) ) ) {

					$template = plugin_dir_path( __FILE__ ) . 'templates/cpt_template_xyz.php';

			}

			

			return $template;

			}
		 */



		// Allow CPT to be added to front page

		function WPSECONDC_add_to_front( $pages ){

			$WPSECONDC_post_type = new WP_Query( array(

				'post_type' =>  array(

					WPSECONDC_CPT_NAME

					)

				) );

			 if ( $WPSECONDC_post_type->post_count > 0 ) {

				 $pages = array_merge( $pages, $WPSECONDC_post_type->posts );

			 }



			return $pages;

		}

		// Stop CPT on front page from redirecting

		function wpsecondc_enable_fps( $query ){



			if('' == $query->query_vars['post_type'] && 0 != $query->query_vars['page_id'])



				$query->query_vars['post_type'] = array( 'page', WPSECONDC_CPT_NAME, WPSECONDC_CPT_NAME );



		}

		

?>