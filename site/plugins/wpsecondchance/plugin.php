<?php
/*
Plugin Name: WPSecondChance
Plugin URI: http://www.wpsecondchance.com/
Description: Change the way you link to other sites.
Version: 1.2
Author: WPSecondChance.com
Author URI: http://www.wpsecondchance.com/

*/
define( 'WPSECONDC_WP_DIR', get_bloginfo('wpurl') );
//define( 'WPSECONDC_TEMPLATES', get_bloginfo('wpurl'). '/modules/templates' );
define( 'WPSECONDC_TEMPLATES', get_bloginfo('wpurl') );
$post_type = get_option('_aspk_wpc_post_type');
if(empty($post_type)){
	define( 'WPSECONDC_NAME', 'wp2c' );
	define( 'WPSECONDC_CPT_NAME', 'wp2c' );
}else{
	define( 'WPSECONDC_NAME', $post_type );
	define( 'WPSECONDC_CPT_NAME', $post_type );
}


define( 'WPSECONDC_CPT_LABEL', 'Campaign' );



//Initialize the metabox class
function wpsecondc_initialize_cmb_meta_boxes() {

 //Funciona OK
   wp_register_script('wpsecondc_script_showhide', plugins_url('showhide.js', __FILE__), array('jquery'),'1.1', true);
   wp_enqueue_script('wpsecondc_script_showhide');

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once(plugin_dir_path( __FILE__ ) . '/lib/init.php');
		
	
}
add_action( 'init', 'wpsecondc_initialize_cmb_meta_boxes', 9999 );
//Add Meta Boxes
function wpsecondc_sample_metaboxes( $meta_boxes ) {
	$prefix = '_wpsecondc_'; // Prefix for all fields
	
	
	
	if (function_exists('wpsecondchance_exitaddon')) {
	
		include ('full_metaboxes.php');
	
	}else{
		include ('partial_metaboxes.php');
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	return $meta_boxes;
	
}
add_filter( 'cmb_meta_boxes', 'wpsecondc_sample_metaboxes' );
include("cpt.php");	//Optional (Can Disable). This line adds a custom post type.
include("cpt_template_loader.php");	//Optional (Can Disable). This line adds a custom post type.
?>