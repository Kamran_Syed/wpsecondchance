<?

$meta_boxes[] = array(
		'id' => 'main_metabox',
		'title' => 'WPSecondChance - Main Settings',
		'pages' => array(WPSECONDC_CPT_NAME), // Add Metabox to this CPT
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			
			
		array(
				'name' => 'Link To:',
				'desc' => 'Enter a url (should begin with http://)',
				'id' => $prefix . 'urlredirect',
				'type' => 'text',
				'value' => 'http://'
			),
			
			
		array(
				'name' => 'Retargeting Code:',
				'desc' => 'Enter your retargeting code. ',
				'id' => $prefix . 'retargetingcode',
				'type' => 'textarea_code'
			),
			
			
		

		
		
				array(
				'name'    => 'Add Exit Popup To Url:',
				'id'      => $prefix . 'show_exit',
				'desc'    => 'Allows you to show an ad when the visitor tries to leave the website. Comment: If a website does (does not show), please select "No".',
				'type'    => 'select',
				'options' => array(
					'no'  => 'No',
					'yes'   => 'Yes',
					
				),
				),
				
		
	
		
		
				
			
			
			
			
			
			
			
			
			
		),
	);
	//Fin Metabox
	
	
	
	
	
	
	
	
	
				$meta_boxes[] = array(
					'id' => 'exit_metabox',
					'title' => 'Exit Popup',
					'pages' => array(WPSECONDC_CPT_NAME), // Add Metabox to this CPT
					'context' => 'normal',
					'priority' => 'high',
					'show_names' => true, // Show field names on the left
					'fields' => array(
						
						
						array(
							'name' => 'Page Title',
							'desc' => 'This title will be visible in the title bar (browser).',
							'id' => $prefix . 'page_title',
							'type' => 'text'
						),
						
						
						
						
						
						array(
							'name' => 'Close Popup Link Text',
							'desc' => 'Example: Close This Window. Comment: If you leave this field empty, this link will not be available to your visitors but they will still be able to close the popup by pressing the ESC key.',
							'id' => $prefix . 'closelinktext',
							'type' => 'text'
						),
						
						
						
						
						array(
							'name' => 'Popup Content',
							'desc' => 'You can enter any content of your choice.',
							'id' => $prefix . 'salespage',
							'type' => 'wysiwyg',
							'options' => array(
								'media_buttons' => true, // show insert/upload button(s)
								'tinymce' => true
							
							),
						),
									
						
						
						
						
						
					),
				);
				//Fin Metabox
				
				
	
	
	
	
?>